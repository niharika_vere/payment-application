package com.squad1.paymentapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.paymentapp.dto.TransactionHistory;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.exception.CustomerNotFound;
import com.squad1.paymentapp.exception.NoTransactionHistory;
import com.squad1.paymentapp.repository.CustomerRepository;
import com.squad1.paymentapp.repository.TransactionRepository;
import com.squad1.paymentapp.service.impl.MonthlyTransactionServiceImpl;
import com.squad1.paymentapp.util.SuccessResponse;

@ExtendWith(SpringExtension.class)
class MonthlyTransactionServiceImplTest {
	@Mock
	private TransactionRepository transactionRepository;

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private MonthlyTransactionServiceImpl monthlyTransactionServiceImpl;

	Customer customer = Customer.builder().aadhar("12345432").balance(1000.0).customerId(1l).email("manu@gmail.com")
			.name("Manoj").build();
	Transaction transaction = Transaction.builder().amount(100.0).category(Category.HOMELOAN).closingBalance(1000.0)
			.customer(customer).dateTime(LocalDateTime.now()).description("sds").transactionId(2l)
			.transactionType(TransactionType.DEBIT).build();
	List<Transaction> transactions = List.of(transaction);

	@Test
	void testMonthlyTransactions() {
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(transactionRepository.findByCustomer(customer)).thenReturn(Optional.of(transactions));
		TransactionHistory transactionHistory = monthlyTransactionServiceImpl
				.getMonthlyTransaction(customer.getCustomerId());
		assertNotNull(transactionHistory);
		assertEquals(SuccessResponse.SUCCESS_CODE3, transactionHistory.statusCode());
	}

	@Test
	void testTransactionHistoryInvalidCustomer() {
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		assertThrows(CustomerNotFound.class, () -> monthlyTransactionServiceImpl.getMonthlyTransaction(1l));

	}

	@Test
	void testNoTransactionHistory() {
		Mockito.when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(transactionRepository.findByCustomer(customer)).thenReturn(Optional.empty());
		assertThrows(NoTransactionHistory.class, () -> monthlyTransactionServiceImpl.getMonthlyTransaction(1l));
	}
}
