package com.squad1.paymentapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.squad1.paymentapp.dto.ApiResponse;
import com.squad1.paymentapp.dto.TransactionRec;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.exception.CustomerNotFound;
import com.squad1.paymentapp.exception.InsufficientFunds;
import com.squad1.paymentapp.repository.CustomerRepository;
import com.squad1.paymentapp.repository.TransactionRepository;
import com.squad1.paymentapp.service.impl.TransactionServiceImpl;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private TransactionRepository transactionRepository;

	@InjectMocks
	private TransactionServiceImpl transactionService;

	@Test
	void transactionWithValidData() {
		Long customerId = 1L;
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customer.setBalance(1000.0);

		TransactionRec transactionRec = new TransactionRec("Test Transaction", 100.0);
		Category category = Category.HOMELOAN;
		TransactionType transactionType = TransactionType.DEBIT;

		when(customerRepository.findById(customerId)).thenReturn(java.util.Optional.of(customer));

		ApiResponse response = transactionService.processTransaction(customerId, category, transactionType,
				transactionRec);

		assertNotNull(response);
		assertEquals("SUCCESS01", response.httpStatus());
		assertEquals("Transaction successfully , Remaining balance :900.0", response.message());
	}

	@Test
	void transactionWithInsufficientFunds() {
		Long customerId = 1L;
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customer.setBalance(100.0);

		TransactionRec transactionRec = new TransactionRec("Test Transaction", 200.0);
		Category category = Category.HOMELOAN;
		TransactionType transactionType = TransactionType.DEBIT;

		when(customerRepository.findById(customerId)).thenReturn(java.util.Optional.of(customer));

		assertThrows(InsufficientFunds.class, () -> {
			transactionService.processTransaction(customerId, category, transactionType, transactionRec);
		});
	}

	@Test
	void transactionWithCustomerNotFound() {

		when(customerRepository.findById(1l)).thenReturn(java.util.Optional.empty());
        TransactionRec transactionRec=TransactionRec.builder().amount(100.0).description("sdsj").build();
		assertThrows(CustomerNotFound.class, () -> transactionService.processTransaction(1l, Category.HOMELOAN,
				TransactionType.DEBIT, transactionRec));
	}

	@Test
	void transactionSaveTransactionDetails() {
		Long customerId = 1L;
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customer.setBalance(1000.0);

		TransactionRec transactionRec = new TransactionRec("Test Transaction", 100.0);
		Category category = Category.HOMELOAN;
		TransactionType transactionType = TransactionType.DEBIT;

		CustomerRepository customerRepository = mock(CustomerRepository.class);
		when(customerRepository.findById(customerId)).thenReturn(java.util.Optional.of(customer));

		TransactionRepository transactionRepository = mock(TransactionRepository.class);

		TransactionServiceImpl transactionService = new TransactionServiceImpl(customerRepository,
				transactionRepository);

		transactionService.processTransaction(customerId, category, transactionType, transactionRec);

		ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);
		verify(transactionRepository).save(transactionCaptor.capture());

		Transaction savedTransaction = transactionCaptor.getValue();
		assertNotNull(savedTransaction);
		assertEquals("Test Transaction", savedTransaction.getDescription());
		assertEquals(100.0, savedTransaction.getAmount());
		assertEquals(transactionType, savedTransaction.getTransactionType());
		assertEquals(category, savedTransaction.getCategory());
		assertEquals(customer, savedTransaction.getCustomer());
		assertNotNull(savedTransaction.getDateTime());
		assertNotNull(savedTransaction.getClosingBalance());
	}

}
