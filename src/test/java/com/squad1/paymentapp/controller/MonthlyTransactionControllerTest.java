package com.squad1.paymentapp.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.paymentapp.dto.TransactionHistory;
import com.squad1.paymentapp.dto.TransactionHistoryRecord;
import com.squad1.paymentapp.service.MonthlyTransactionService;
import com.squad1.paymentapp.util.SuccessResponse;

@ExtendWith(SpringExtension.class)
class MonthlyTransactionControllerTest {
	@Mock
	private MonthlyTransactionService monthlyTransactionService;
	@InjectMocks
	private MonthlyTransactionController monthlyTransactionController;

	@Test
	void testMonthlyTransactions() {
		List<TransactionHistoryRecord> historyRecords = List.of(TransactionHistoryRecord.builder().customerId(23l)
				.closingBalance(900.0).month("Jan 2022").totalIncome(100.0).totalOutcome(100.0).build());
		TransactionHistory transactionHistory = TransactionHistory.builder().transactionHistory(historyRecords)
				.statusCode(SuccessResponse.SUCCESS_CODE3).build();
		Mockito.when(monthlyTransactionService.getMonthlyTransaction(1l)).thenReturn(transactionHistory);
		ResponseEntity<TransactionHistory> responseEntity = monthlyTransactionController.getMonthlyTransaction(1l);
		assertNotNull(responseEntity);
	}
}
