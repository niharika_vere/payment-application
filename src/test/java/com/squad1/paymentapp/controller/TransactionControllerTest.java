package com.squad1.paymentapp.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.squad1.paymentapp.dto.ApiResponse;
import com.squad1.paymentapp.dto.TransactionRec;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.service.TransactionService;

@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {

	@Mock
	private TransactionService transactionService;

	@InjectMocks
	private TransactionController transactionController;

	 @Test
	     void testProcessTransaction() {
	        Long customerId = 123L;
	        Category category = Category.EMI;
	        TransactionType transactionType = TransactionType.DEBIT;
	        TransactionRec transactionRec = new TransactionRec("Test transaction", 50.0);
	        ApiResponse expectedApiResponse = ApiResponse.builder().message("Transaction successful").httpStatus("200").build();
	 
	        when(transactionService.processTransaction(customerId, category, transactionType, transactionRec))
	            .thenReturn(expectedApiResponse);
	 
	        ResponseEntity<ApiResponse> responseEntity = transactionController.processTransaction(customerId, category, transactionType, transactionRec);
	 
	        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	        assertEquals(expectedApiResponse, responseEntity.getBody());
	    }
	
}
