package com.squad1.paymentapp.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record TransactionHistory(List<TransactionHistoryRecord> transactionHistory, String message,String statusCode) {

}
