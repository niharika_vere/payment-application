package com.squad1.paymentapp.dto;

import lombok.Builder;


@Builder
public record ApiResponse(String message,String httpStatus) {

}
