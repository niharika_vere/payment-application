package com.squad1.paymentapp.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record TransactionRec(@NotNull(message = "Description should not be null") String description,
		@Min(value = 1, message = "minimum balance must be 1 rupee ") @Max(value = 10000, message = "maximum limit is 10000 ") Double amount) {

}
