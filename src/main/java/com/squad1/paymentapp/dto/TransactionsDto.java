package com.squad1.paymentapp.dto;

import java.time.LocalDateTime;

import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.TransactionType;

import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
public record TransactionsDto(String description
	,Double amount,TransactionType transactionType
	,Category category,Long customerId ,LocalDateTime dateTime,Double closingBalance){

}
