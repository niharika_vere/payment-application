package com.squad1.paymentapp.dto;

import lombok.Builder;

@Builder
public record TransactionHistoryRecord(String month, Long customerId, Double totalIncome, Double totalOutcome,
		Double closingBalance) {

}
