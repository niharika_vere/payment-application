package com.squad1.paymentapp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.paymentapp.dto.ApiResponse;
import com.squad1.paymentapp.dto.TransactionRec;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class TransactionController {

	private final TransactionService transactionService;

	/**
	 * 
	 * Making transaction by using customerId
	 * 
	 * @param customerId      verifying customer
	 * @param category        choosing category
	 * @param transactionType choosing transactionType
	 * @param transactionRec  taking input from user
	 * @return giving response of transaction
	 * 
	 */
	@PostMapping("/transactions/customers/{customerId}")
	public ResponseEntity<ApiResponse> processTransaction(@PathVariable Long customerId,
			@RequestParam Category category, @RequestParam TransactionType transactionType,
			@RequestBody @Valid TransactionRec transactionRec) {
		ApiResponse response = transactionService.processTransaction(customerId, category, transactionType,
				transactionRec);
		log.info("Transaction iniciated");
		return ResponseEntity.ok(response);

	}

}
