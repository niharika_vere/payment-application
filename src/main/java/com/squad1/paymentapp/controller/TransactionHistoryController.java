package com.squad1.paymentapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.paymentapp.dto.TransactionResponse;
import com.squad1.paymentapp.service.TransactionHistoryService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/transactions/customers/{customerId}")
public class TransactionHistoryController {
	private final TransactionHistoryService transactionHistoryService;

	@GetMapping
	public ResponseEntity<TransactionResponse> getTransactionsByCustomerId(@PathVariable Long customerId,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(transactionHistoryService.getTransactionsByCustomerId(customerId, page, size));
	}
}