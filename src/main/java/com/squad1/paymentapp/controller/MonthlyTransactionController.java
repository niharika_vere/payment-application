package com.squad1.paymentapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.paymentapp.dto.TransactionHistory;
import com.squad1.paymentapp.service.MonthlyTransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class MonthlyTransactionController {

	private final MonthlyTransactionService monthlyTransactionService;

	/**
	 * 
	 * @param customerId as a pathVariable to fetch transactions
	 * @param pageNumber to limit number Of Transactions history on particular page
	 * @param pageSize   to limit number of Transaction history are to be displayed
	 *                   in one fetch
	 * @return it will display the list of transaction history by filtering monthly
	 *         income and outcome of each month
	 */

	@GetMapping("/monthly/transactions/customers/{customerId}")
	public ResponseEntity<TransactionHistory> getMonthlyTransaction(@PathVariable(required = true) Long customerId) {
		return ResponseEntity.status(HttpStatus.OK).body(monthlyTransactionService.getMonthlyTransaction(customerId));
	}

}
