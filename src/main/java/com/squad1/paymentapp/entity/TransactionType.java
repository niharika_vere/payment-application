package com.squad1.paymentapp.entity;

public enum TransactionType {

	CREDIT, DEBIT

}
