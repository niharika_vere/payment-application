package com.squad1.paymentapp.entity;

public enum Category {

	HOMELOAN, VEHICLELOAN, EMI, INCOMESOURCE1, INCOMESOURCE2

}
