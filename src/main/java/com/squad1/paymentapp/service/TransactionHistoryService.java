package com.squad1.paymentapp.service;

import java.util.List;

import com.squad1.paymentapp.dto.TransactionResponse;
import com.squad1.paymentapp.dto.TransactionsDto;

public interface TransactionHistoryService {

	

	TransactionResponse  getTransactionsByCustomerId(Long customerId, int page, int size);

}
