package com.squad1.paymentapp.service;

import com.squad1.paymentapp.dto.TransactionHistory;

public interface MonthlyTransactionService {

	TransactionHistory getMonthlyTransaction(Long customerId);

}
