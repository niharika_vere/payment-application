package com.squad1.paymentapp.service;

import com.squad1.paymentapp.dto.ApiResponse;
import com.squad1.paymentapp.dto.TransactionRec;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.TransactionType;

public interface TransactionService {

	ApiResponse processTransaction(Long customerId, Category category, TransactionType transactionType,
			TransactionRec transactionRec);

}
