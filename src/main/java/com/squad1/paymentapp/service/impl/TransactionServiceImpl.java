package com.squad1.paymentapp.service.impl;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.squad1.paymentapp.dto.ApiResponse;
import com.squad1.paymentapp.dto.TransactionRec;
import com.squad1.paymentapp.entity.Category;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.exception.CustomerNotFound;
import com.squad1.paymentapp.exception.InsufficientFunds;
import com.squad1.paymentapp.repository.CustomerRepository;
import com.squad1.paymentapp.repository.TransactionRepository;
import com.squad1.paymentapp.service.TransactionService;
import com.squad1.paymentapp.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;



@RequiredArgsConstructor
@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService {

	private final CustomerRepository customerRepository;

	private final TransactionRepository transactionRepository;

	@Override
	@Transactional
	public ApiResponse processTransaction(Long customerId, Category category, TransactionType transactionType,
			TransactionRec transactionRec) {
		Customer customer = customerRepository.findById(customerId).orElseThrow(() -> {
			log.error("Customer not found");
			throw new CustomerNotFound();
		});

		Double currentBalance = customer.getBalance();
		Double transactionAmount = transactionRec.amount();

		if (transactionType == TransactionType.DEBIT && currentBalance < transactionAmount) {
			log.error("Insufficient funds");
			throw new InsufficientFunds();
		}

		if (transactionType == TransactionType.DEBIT) {
			currentBalance -= transactionAmount;
		} else {
			currentBalance += transactionAmount;
		}

		Transaction transaction = Transaction.builder().description(transactionRec.description())
				.amount(transactionAmount).transactionType(transactionType).category(category).customer(customer)
				.dateTime(LocalDateTime.now()).closingBalance(currentBalance).build();
		log.info("updating trasaction details" + transaction);
		transactionRepository.save(transaction);

		customer.setBalance(currentBalance);

		log.info("Customer account updated" + customer);
		customerRepository.save(customer);

		log.info("Transaction successful");
		return ApiResponse.builder().message(SuccessResponse.SUCCESS_MESSAGE1 + currentBalance)
				.httpStatus(SuccessResponse.SUCCESS_CODE1).build();
	}
}
