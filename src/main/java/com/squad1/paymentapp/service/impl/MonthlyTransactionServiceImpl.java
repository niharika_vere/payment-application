package com.squad1.paymentapp.service.impl;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.squad1.paymentapp.dto.TransactionHistory;
import com.squad1.paymentapp.dto.TransactionHistoryRecord;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;
import com.squad1.paymentapp.entity.TransactionType;
import com.squad1.paymentapp.exception.CustomerNotFound;
import com.squad1.paymentapp.exception.NoTransactionHistory;
import com.squad1.paymentapp.repository.CustomerRepository;
import com.squad1.paymentapp.repository.TransactionRepository;
import com.squad1.paymentapp.service.MonthlyTransactionService;
import com.squad1.paymentapp.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class MonthlyTransactionServiceImpl implements MonthlyTransactionService {
	private final CustomerRepository customerRepository;
	private final TransactionRepository transactionRepository;

	@Override
	public TransactionHistory getMonthlyTransaction(Long customerId) {
		Customer customer = customerRepository.findById(customerId).orElseThrow(() -> {
			log.error("No customer found for id:" + customerId);
			throw new CustomerNotFound();
		});
		List<Transaction> transactions = transactionRepository.findByCustomer(customer).orElseThrow(() -> {
			log.error("No transaction history for the Customer:" + customerId);
			throw new NoTransactionHistory();
		});
		Map<YearMonth, List<Transaction>> monthlyTransactions = transactions.stream()
				.collect(Collectors.groupingBy(t -> YearMonth.from(t.getDateTime())));

		List<TransactionHistoryRecord> monthlyTransactionHistoryRecords = new ArrayList<>();

		monthlyTransactions.forEach((yearMonth, monthlyTransactionList) -> {
			Double totalIncome = calculateTotalIncome(monthlyTransactionList);
			Double totalOutcome = calculateTotalOutcome(monthlyTransactionList);
			Double closingBalance = calculateClosingBalance(monthlyTransactionList);
			monthlyTransactionHistoryRecords
					.add(new TransactionHistoryRecord(yearMonth.format(DateTimeFormatter.ofPattern("MMM-yyyy")),
							customerId, totalIncome, totalOutcome, closingBalance));
		});

		log.info("Monthly transaction history for customer computed and sent");
		return TransactionHistory.builder().transactionHistory(monthlyTransactionHistoryRecords)
				.statusCode(SuccessResponse.SUCCESS_CODE3).message(SuccessResponse.SUCCESS_MESSAGE3).build();
	}

	private Double calculateTotalIncome(List<Transaction> transactions) {
		return transactions.stream().filter(t -> t.getTransactionType().equals(TransactionType.CREDIT))
				.mapToDouble(Transaction::getAmount).sum();
	}

	private Double calculateTotalOutcome(List<Transaction> transactions) {
		return transactions.stream().filter(t -> t.getTransactionType().equals(TransactionType.DEBIT))
				.mapToDouble(Transaction::getAmount).sum();
	}

	private Double calculateClosingBalance(List<Transaction> transactions) {
		return transactions.isEmpty() ? 0.0 : transactions.get(transactions.size() - 1).getClosingBalance();
	}
}
