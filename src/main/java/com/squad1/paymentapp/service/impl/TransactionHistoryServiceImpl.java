package com.squad1.paymentapp.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.squad1.paymentapp.dto.TransactionResponse;
import com.squad1.paymentapp.dto.TransactionsDto;
import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;
import com.squad1.paymentapp.exception.CustomerNotFound;
import com.squad1.paymentapp.exception.NoTransactionHistory;
import com.squad1.paymentapp.repository.CustomerRepository;
import com.squad1.paymentapp.repository.TransactionRepository;
import com.squad1.paymentapp.service.TransactionHistoryService;
import com.squad1.paymentapp.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionHistoryServiceImpl implements TransactionHistoryService {
	private final TransactionRepository transactionRepository;
	private final CustomerRepository customerRepository;

	public TransactionResponse getTransactionsByCustomerId(Long customerId, int page, int size) {
		Customer customer=customerRepository.findById(customerId).orElseThrow(()-> new CustomerNotFound());
		Pageable pageable = PageRequest.of(page, size);
		Page<Transaction> transactions = transactionRepository.findByCustomer(customer, pageable);
		if (transactions.getContent().isEmpty()) {
			log.error("No transactions found for customer ID: {}", customerId);
			throw new NoTransactionHistory();
		}
		return TransactionResponse.builder().transactions(transactions.getContent().stream().map(i ->{
			return TransactionsDto.builder().amount(i.getAmount()).category(i.getCategory()).closingBalance(i.getClosingBalance()).description(i.getDescription()).transactionType(i.getTransactionType()).dateTime(i.getDateTime())
					.customerId(customerId).build();
		}).toList()).statusCode(SuccessResponse.SUCCESS_CODE2).message(SuccessResponse.SUCCESS_MESSAGE2).build();

	}
}
