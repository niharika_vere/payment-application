package com.squad1.paymentapp.util;

public interface SuccessResponse {

	/**
	 * Success Response
	 */

	String SUCCESS_CODE1 = "SUCCESS01";
	String SUCCESS_MESSAGE1 = "Transaction successfully , Remaining balance :";

	String SUCCESS_CODE2 = "SUCCESS02";
	String SUCCESS_MESSAGE2 = " Transaction History fetched successfully";

	String SUCCESS_MESSAGE3="Monthly transaction summary of customer";
	String SUCCESS_CODE3 = "SUCCESS03";

}
