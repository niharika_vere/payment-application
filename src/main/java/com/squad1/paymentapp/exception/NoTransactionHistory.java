package com.squad1.paymentapp.exception;

public class NoTransactionHistory extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoTransactionHistory(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public NoTransactionHistory() {
		super("No Transaction History", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
