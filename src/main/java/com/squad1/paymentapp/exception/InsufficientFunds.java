package com.squad1.paymentapp.exception;

public class InsufficientFunds extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientFunds(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InsufficientFunds() {
		super("Insufficient Funds", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
