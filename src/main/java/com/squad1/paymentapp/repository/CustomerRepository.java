package com.squad1.paymentapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.paymentapp.entity.Customer;



public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
