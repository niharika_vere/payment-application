package com.squad1.paymentapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.paymentapp.entity.Customer;
import com.squad1.paymentapp.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	Page<Transaction> findByCustomer(Customer customer, Pageable pageable);

	Optional<List<Transaction>> findByCustomer(Customer customer);

}
